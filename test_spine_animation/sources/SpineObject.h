#include "W4Framework.h"

W4_USE_UNSTRICT_INTERFACE

enum class SpriteOrientation {
    left,
    right
};

enum class AnimationSequence {
    idle,
    hit,
    run,
    win
};

class SpineObject
{
    sptr<Spine> m_spine;

    float m_scale = 2.5f;
    vec3 m_position;

    SpriteOrientation m_orientation = SpriteOrientation::right;
    AnimationSequence m_currentAnimation = AnimationSequence::idle;
    float m_animationTime = 0.f;
    bool m_animationOver = false;
    bool m_looped = true;

public:
    SpineObject(const std::string& atlasName,
        const std::string& jsonName,
        const std::string& spineName,
        vec3 position,
        SpriteOrientation orientation)
        :
        m_spine{ w4::make::sptr<Spine>(spineName, atlasName, jsonName) },
        m_position{ position }
    { 
        m_spine->setDepthTestFlag(false);
        m_spine->timeScale = 1.5f;
        m_spine->setUsePremultipliedAlpha(true);
        m_spine->skeleton->setPosition(position.x, position.y);
        m_spine->setLocalScale(vec3(m_scale, m_scale, m_scale));

        Render::getRoot()->addChild(m_spine);
        
        SetOrientation(orientation);
        ChangeAnimationSequence(AnimationSequence::idle);
    }

    ~SpineObject() {
        Render::getRoot()->removeChild(m_spine);
    }

    void ChangeAnimationSequence(AnimationSequence animation)
    {
        switch (animation)
        {
        case AnimationSequence::idle:
            m_spine->state->setAnimation(0, "idle", false);
            break;
        case AnimationSequence::hit:
            m_spine->state->setAnimation(0, "hit", false);
            break;
        case AnimationSequence::run:
            m_spine->state->setAnimation(0, "run", false);
            break;
        case AnimationSequence::win:
            m_spine->state->setAnimation(0, "win", false);
            break;
        }
    }

    void ReplayAnimation() {
        m_animationTime = 0.f; 
        m_animationOver = true;
        ChangeAnimationSequence(m_currentAnimation);
    }

    void ProcessAnimation(float dt)
    {
        spine::TrackEntry* entry = m_spine->state->getCurrent(0);
        if (!entry) {
            return;
        }

        m_animationOver = entry->isComplete();

        if (m_animationOver && m_looped) {
            ReplayAnimation();
        }
    }

    void UpdateSpine(float dt) {
        m_spine->onUpdate(dt);
    }

    void SetOrientation(SpriteOrientation orientation)
    {
        m_orientation = orientation;

        switch (orientation) {
        case SpriteOrientation::left:
            m_spine->skeleton->setScaleX(-1);
            break;
        case SpriteOrientation::right:
            m_spine->skeleton->setScaleX(1);
            break;
        }

        UpdateSpine(1.f / 60.f);
    }
};