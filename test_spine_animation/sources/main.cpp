#include "W4Framework.h"
#include "SpineObject.h"
#include <vector>
#include <algorithm>

W4_USE_UNSTRICT_INTERFACE



struct TestSpineAnimation : public IGame
{
private:
    static constexpr size_t OBJECTS_IN_ROW = 10;
    uint centerX;

    SpriteOrientation spineObjectsOrientation = SpriteOrientation::right;
    int m_spineObjectsNum = 0;

    std::vector<std::vector<SpineObject>> m_spineObjects; 

public:
    void onStart() override
    {
        centerX = gui::getVirtualResolution().w / 2;

        CreateButtonIncreaseSpine();
        CreateButtonDecreaseSpine();  
        CreateSpineObjectsRow();
    }

    void onUpdate(float dt) override
    {
        ShowFPS();
        ShowElapsedTime();
        ShowSpineObjectsNum();
         
        for (auto& vecSpineObjects : m_spineObjects) {
            for (auto& spineObject : vecSpineObjects) {
                spineObject.UpdateSpine(dt);
                spineObject.ProcessAnimation(dt);
            }
        }
    }

private: 
    void CreateSpineObjectsRow()
    {
        static const std::string atlasName = "galahad.atlas";
        static const std::string jsonName = "galahad.json";
        static const std::string spineName = "galahad_spine"; 

        std::vector<SpineObject> spineRow;
        spineRow.reserve(OBJECTS_IN_ROW);
         
        int currentPosY = m_spineObjects.size();
          
        float sizeX = 500.f;
        float sizeY = 500.f;
        float offsetX = centerX - ((OBJECTS_IN_ROW + 1) / 2) * sizeX;
        float offsetY = - sizeY * currentPosY;

        for (int i = 0; i < OBJECTS_IN_ROW; ++i) { 
            vec3 pos{offsetX + i * sizeX, offsetY - sizeY, 0 };
            spineRow.emplace_back(atlasName, jsonName, spineName, pos, spineObjectsOrientation);
        }
         
        m_spineObjects.push_back(std::move(spineRow));
    }
    void DeleteSpineObjectsRow() {
        if (!m_spineObjects.empty()) {
            m_spineObjects.pop_back();
        }
    } 

    // buttons
    void CreateButtonIncreaseSpine() 
    {
        auto buttonIncreaseSpine = createWidget<Button>(nullptr, "Spine +10", ivec2(centerX, 350));
        buttonIncreaseSpine->setHorizontalAlign(HorizontalAlign::Center);
        buttonIncreaseSpine->onTap([&] { CreateSpineObjectsRow(); });
    }
    void CreateButtonDecreaseSpine()
    {
        auto buttonDecreaseSpine = createWidget<Button>(nullptr, "Spine -10", ivec2(centerX, 450));
        buttonDecreaseSpine->setHorizontalAlign(HorizontalAlign::Center);
        buttonDecreaseSpine->onTap([&]() { DeleteSpineObjectsRow(); });
    }

    // labels
    void ShowFPS()
    {
        static auto labelFPS = createWidget<Label>(nullptr, "", ivec2(centerX, 50));
        int fps = static_cast<int>(1.0f / Timer::getDeltaTime());
        labelFPS->setText(utils::format("FPS: %d", fps));
    }

    void ShowElapsedTime()
    {
        static auto labelElapsedTime = createWidget<Label>(nullptr, "", ivec2(centerX, 150));
        int elapsedTime = static_cast<int>(Render::getElapsedTime());
        labelElapsedTime->setText(utils::format("ElapsedTime: %d sec", elapsedTime));
    }

    void ShowSpineObjectsNum()
    {
        static auto labelSpineObjectsNum = createWidget<Label>(nullptr, "", ivec2(centerX, 250)); 
        labelSpineObjectsNum->setText(utils::format("Spine objects number: %d", m_spineObjects.size() * OBJECTS_IN_ROW));
    }
};

W4_RUN(TestSpineAnimation)
